---
layout: cover
background: /background-1920x1080.jpg
download: /slides.pdf
---

# C++20

---

# Programme

<div class="grid grid-cols-2">

- Introduction
  - Pré-requis
  - Code repository
- Retour sur l'historique du C++
- Les ajouts majeurs du C++20
  - Concepts
  - Ranges
  - Coroutines
  - Modules
- Autres ajouts
  - Ajouts au langage
  - Ajouts à la STL
- Mise en pratique: exercices et mini-projet

<div>

```cpp
import <iostream>;
import <format>;
import <thread>;
import <atomic>;

static constinit auto version = 20u;

int main(int argc, const char* argv[]) {
  std::atomic<bool> flag1, flag2;
  std::jthread thread([&]() {
    while (!flag1.load()) {};
    std::cout << std::format("C++ {}", version);
    flag2.store(true);
  });
  std::cout << "Hello ";
  flag1.store(true);
  while (!flag2.load()) {};
  std::cout << " !\n";
  return 0;
}
```

</div>

</div>

---
layout: intro
class: text-center
---

# Introduction

---

# Présentation

- Laurent Noël
- Anciennement développeur 3D R&D
- Maintenant: développeur freelance, orienté blockchain
- Langages principaux: C/C++, Python, langages du Web, Solidity

---

# Pré-requis

- Une bonne connaissance du C++
- Environnement de développement
  - Bash
  - Git
  - CMake
  - Un IDE (Visual Studio Code conseillé)

---

# Code repository

- https://gitlab.com/c2ba-learning/cpp-20/code-template
- Contient un projet CMake avec dépendances gérées via vcpkg
- Une CI/CD Gitlab
- Multiplatforme (testé Linux GCC et Windows MSVC)
- Une config VSCode pour intégration CMake et Debug interactif
- Examples de code C++20
- Exercices et corrections
- Voir le README pour mise en place

---
layout: intro
class: text-center
---

# Historique du C++

---

# Legacy C++

- 1979: Bjarne Stroustrup commence le développement de **C with Classes**
- 1985: Première édition de The C++ Programming Language
- C++98
  - Première specification du langage
  - STL: conteneurs, algorithmes, string, I/O
  - Templates
- C++03
  - Corrections mineures
- Standardisation du langage
  - Comitté d'experts et d'entreprises
  - Priorité absolue sur les performance
  - Préférence ajout STL avant modification du langage

---

# 2005: TR1

- Pose les fondations du C++ moderne
- Basé sur la bibliothèque boost
- Spécification de 13 bibliothèques destinées à être intégrés au C++11

---

# C++11

<div class="grid grid-cols-2">

<div>

Améliorations du langage

- Move semantics
- Initialisation unifiée
- Mots clefs `auto`, `decltype`, `constexpr`
- Lambda functions

</div>
<div>

Améliorations de la STL

- Multithreading standard avec `<thread>`
- Regexps avec `<regexp>`
- Smart pointers: `std::unique_ptr`, `std::shared_ptr`
- `std::array`, `std::unordered_map`

</div>
</div>

---

# C++14 et C++17

<div class="grid grid-cols-2">

<div>

C++14

- Locks reader-writer
- Generic lambda functions

</div>
<div>

C++17

- Fold expressions
- `constexpr if`
- Structures binding
- `std::string_view`
- Algorithmes parallèles dans la STL
- `<filesystem>`
- `std::any`, `std::optional`, `std::variant`

</div>
</div>

---

# C++20

<div class="grid grid-cols-3">

<div>

4 grandes evolutions

- Concepts
- Modules
- Ranges
- Coroutines

</div>

<div>

Améliorations du langage

- Spaceship operator
- Designated Initialization
- `consteval`, `constinit`

</div>

<div>

Amélioration de la STL

- `std::span`
- Utilitaires arithmétiques
- Formattage avec `<format>`
- Calendrier et Time Zones
- Primitives de synchronisation avancées

</div>

</div>

---
layout: intro
class: text-center
---

# Concepts

---

# Meta-programmation en C++

- Les templates en C++ permettent la programmation générique à compile-time
- Difficile, couteux, dur à maintenir
- Souvent reservé au code de bibliothèques dediées (boost)
- Syntaxe non adaptée
  - Messages d'erreurs complexe
  - Nécessité d'utiliser des méthodes détournées (spécialisation de templates)

---

# Historique des concepts en C++

- L'objectif des concepts (et autres outils à compile-time) est de faciliter la meta-programmation
  - Pré-requis sur les types
  - Choix de branche de code à compile-time en fonction des types
  - Syntaxe simple d'utilisation
- Initialement prévu pour C++11
  - Design initial trop complexe
  - Deuxième essai: Concepts Lite prévu pour C++17, supprimé
  - Après plus de 10 ans de recherche, intégrés à C++20

---

# Qu'est ce qu'un concept ?

- Mathématiquement, un concept est un ensemble de pré-requis sur un type ou une valeur
  - syntaxe
  - sémantique
  - complexité en temps ou en espace

---

# Qu'est ce qu'un concept en C++20 ?

- Ensemble de pré-requis syntaxique
- Une interface sur un template (type ou littéral)
- Vérifiée à compile-time
- Exprimée sous forme de prédicat (fonction booléenne)

---

# Définir et utiliser un concept

```cpp
// Forme générale:
template<typename T1, typename T2>
concept MyAwesomeConcept = Predicate;

template<typename T1, typename T2>
requires MyAwesomeConcept<T1, T2>
void my_awesome_function(T1&& value1, T2&& value2) {
  // do something with values
}
```

`Predicate` peut être remplacée par n'importe quelle expression booléenne évaluée à compile time

---

# Exemple simple

```cpp
template <typename T>
concept AcceptAll = true;

template <typename T>
concept AcceptNothing = false;

template <typename T>
requires AcceptAll<T>
void always_compile(T &&value) {}

template <typename T>
requires AcceptNothing<T>
void never_compile(T &&value) {}

void basic_concepts() {
  auto the_answer = 42;

  always_compile(the_answer);

  // never_compile(the_answer);
  // error C7602: 'never_compile': the associated constraints are not satisfied
}
```

---

# Concepts à partir de type traits

```cpp
#include <type_traits>

template <typename T>
concept Integral = std::is_integral<T>::value;

template <typename T>
concept SignedIntegral = Integral<T> && std::is_signed<T>::value;

template <typename T>
concept UnsignedIntegral = Integral<T> && !SignedIntegral<T>;

void test_from_type_traits() {
  static_assert(Integral<int> && Integral<unsigned int> && Integral<char> && Integral<bool>);
  static_assert(SignedIntegral<int> && !SignedIntegral<unsigned int> && !Integral<float>);
  static_assert(UnsignedIntegral<unsigned char> && !UnsignedIntegral<char>);
}
```

---

# Concepts de la STL

- La STL définie beaucoup de concepts standards
- Permet d'en définir de nouveau en les combinants
- Le header `<concepts>`
  - `std::default_initializable<T>`, `std::derived_from<T, U>`, `std::convertible_to<T, U>`, ...
  - `std::integral<T>`, `std::signed_integral<T>`, `std::unsigned_integral<T>`, `std::floating_point<T>`
  - `std::destructible<T>`, `std::default_constructible<T>`, ...
  - `std::movable<T>`, `std::copyable<T>`, `std::regular<T>`, `std::semiregular<T>`, ...
  - `std::invocable<T>`, `std::predicate<T>`, ...
- header `<compare>`: `std::three_way_comparable<T>`
- header `<iterator>`: `std::input_output_iterator<T>`, `std::input_iterator<T>`, ...

---

# Combiner des concepts

```cpp
template <typename T>
concept BuiltinNumber =
    (std::integral<T> || std::floating_point<T>)&&!std::same_as<T, bool>;

template <typename T>
requires BuiltinNumber<T> T builtin_number_times_2(const T &value) {
  return 2 * value;
}

void test_number() {
  const int x = 42;
  builtin_number_times_2(x);

  const double y = 3.14;
  builtin_number_times_2(y);

  const bool b = true;
  // builtin_number_times_2(b); does not compile
}
```

---

# Utiliser un concept à compile-time

- Un concept peut être directement utilisé dans une expression évaluée à compile-time comme un `if constexpr` ou un `static_assert`

```cpp
template <typename T>
requires BuiltinNumber<T>
constexpr T builtin_number_times_2_optimized(const T &value) {
  if constexpr (std::integral<T>) {
    return value << 1;
  } else {
    return 2 * value;
  }
}

void test_number_at_compile_time() {
  static_assert(builtin_number_times_2_optimized(42) == 84);
  static_assert(builtin_number_times_2_optimized(42.0) == 84.0);
}
```

---

# Expressions `requires`

- `requires` permet de construire des concepts (et des tests à compile time) plus avancées

```cpp
requires (parameter-list(optional)) {requirement-seq}
```

---

# Exemples

```cpp
template<typename T>
concept Addable = requires (T a, T b) {
  a + b;
};

template<typename T>
concept Equal = requires(T a, T b) {
  { a == b } -> std::convertible_to<bool>;
  { a != b } -> std::convertible_to<bool>;
};

template<typename T>
concept TypeRequirement = requires {
  typename T::value_type;
  typename Other<T>;
}

```

---

# Différentes syntaxes

- On peut combiner un concept avec `auto` pour définir une fonction template

```cpp
#include <concepts>

template <typename T>
requires std::integral<T>
T gcd(T a, T b) {
  // ...
}

template <typename T> T gcd1(T a, T b) requires std::integral<T> {
  // ...
}

template <std::integral T> T gcd2(T a, T b) {
  // ...
}

std::integral auto gcd3(std::integral auto a, std::integral auto b) {
  // ...
}

const auto gcd4 = [](std::integral auto a, std::integral auto b) -> std::integral auto {}

```

---
layout: intro
class: text-center
---

# Ranges

---

# Historique des ranges

- Bibliothèque d'algorithmes agissant sur des ranges plutot que sur des itérateurs
- Principalement dévelopée par Eric Niebler
- Proposée pour être introduite dans la STL en 2014 (Proposal D4128)

---

# Pourquoi les ranges ?

- Programmation fonctionnelle
- Lazy evaluation
- Example:
```cpp
const std::vector<int> numbers = {1, 2, 3, 4, 5, 6};
const auto results = numbers
  | std::views::filter([](int n){ return n % 2 == 0; })
  | std::views::transform([](int n){ return n * 2; });
for (auto v: results) {
  std::cout << v << " "; // 4 8 12
}
```

---

# Qu'est ce qu'une range ?

- Une range est un type respectant le concept suivant:
```cpp
template<typename T>
concept range = requires(T& t) {
  ranges::begin(t);
  ranges::end(t);
};
```
- `ranges::begin` et `ranges::end` sont définis par défaut via `std::begin` et `std::end`
- A partir de C++20, le type de `end` n'a plus à être le même que `begin`
- `begin` est un itérateur, `end` une sentinelle
- L'operateur `==` doit être définie entre le type de `end` et le type de l'itérateur

---

# Views

- Une range ne possédant pas les elements sur lesquels on itère
- En général une operation permettant de filtrer ou transformer une autre range
```cpp
template<typename T>
concept view = range<T> &&
  movable<T> &&
  default_initializable<T> &&
  enable_view<T>;

template<class T>
inline constexpr bool enable_view = derived_from<T, view_base>;
```
- Exemples:
```cpp
const std::vector<int> numbers = {1, 2, 3, 4, 5, 6};
const auto results = numbers
  | std::views::filter([](int n){ return n % 2 == 0; })
  | std::views::transform([](int n){ return n * 2; });
```
```cpp
const auto numbers = {1, 2, 3, 4, 5};
auto lastTwo = numbers | std::views::drop(3);
for (const auto x : lastTwo) {
  std::cout << x << std::endl; // 4, 5
}
```

---

# Views de la STL

- `std::views::filter`
- `std::views::transform`
- `std::views::take`, `std::views::drop`
- `std::views::take_while`, `std::views::drop_while`
- `std::views::join`, `std::views::split`
- `std::views::reverse`
- `std::views::keys`, `std::views::values`
- `std::views::iota`

---

# Ownership

- Attention: les views ne possède par leur donnée
- Il faut donc que la partie la plus à gauche de la chaine de `|` ne soit pas un temporaire

```cpp
const auto numbers = {1, 2, 3, 4, 5};
auto firstThree = numbers | std::views::take(3);
// auto firstThree = {1, 2, 3, 4, 5} | std::views::take(3); ERROR
```

---

# Namespace `std::ranges` et algorithmes

- Les algorithmes de `<algorithm>` et `<memory>` ont été adapté pour proposer une version opérant sur une range
- Exemple:
```cpp
std::vector<int> myVec{-3, 5, 0, 7, -4};
std::sort(myVec.begin(), myVec.end()); // Avant C++20
std::ranges::sort(myVec); // A partir de C++20
```

---

# Projections

- Il est possible d'utiliser les deux derniers argument de `sort` pour spécifier la fonction d'ordre, et sur quel champs trier
- Exemple:
```cpp
struct PhoneBookEntry{
  std::string name;
  int number;
};

int main() {
  std::vector<PhoneBookEntry> phoneBook{
    {"Brown", 111}, {"Smith", 444}, {"Grimm", 666},
    {"Butcher", 222}, {"Taylor", 555}, {"Wilson", 333}
  };
  std::ranges::sort(phoneBook, {}, &PhoneBookEntry::name); // ascending by name
  std::ranges::sort(phoneBook, std::ranges::greater() , &PhoneBookEntry::name); // descending by name
  std::ranges::sort(phoneBook, {}, &PhoneBookEntry::number); // ascending by number
  std::ranges::sort(phoneBook, std::ranges::greater(), &PhoneBookEntry::number); // descending by number

  std::ranges::sort(phoneBook, {}, [](auto p){ return std::size(p.name) + p.number; } ); // can also use a functor
}
```

---

# Views sur les clefs et les valeurs d'une map

```cpp
std::unordered_map<std::string, int> freqWord{
  {"witch", 25}, {"wizard", 33},
  {"tale", 45}, {"dog", 4},
  {"cat", 34}, {"fish", 23}
};
for (const auto& name : std::views::keys(freqWord)){
  std::cout << name << " ";
}
for (const auto& value : std::views::values(freqWord)){
  std::cout << value << " ";
}
```

---

# Notation fonctionelle et notation en pipeline

```cpp
// Fonctionel:
for (const auto& name : std::views::keys(freqWord)){
  std::cout << name << " ";
}
for (const auto& value : std::views::values(freqWord)){
  std::cout << value << " ";
}
// Pipeline:
for (const auto& name : freqWord | std::views::keys){
  std::cout << name << " ";
}
for (const auto& value : freqWord | std::views::values){
  std::cout << value << " ";
}
```
- Les view implementent `auto operator()<_Rng>(_Rng &&_Range) const`
- L'operateur `auto operator |(_Rng &&_Range, View & view)` est également définie
- `R | V1 | V2 | V3` est équivalent à `V3(V2(V1(R)))`

---

# Lazy Evaluation et Pulling

- Dans l'expression `A | B`, `B` tire ses valeurs de `A` de manière lazy
- `A` peut donc fournir une infinité de valeur, si `B` en extrait une valeur finie on pourra itérer sur le résultat
- Exemple:
```cpp
for (int i: std::views::iota(0) | std::views::take(10)) {
  std::cout << i << "\n";
}
```

---

# Définir une view

- On peut hériter de `std::ranges::view_interface` pour facilement implémenter une view:
```cpp
class MyView : public std::ranges::view_interface<MyView> { // Curiously Recurring Template Pattern
public:
  auto begin() const { /*...*/ }
  auto end() const { /*...*/ }
};
```

---

# La lib ranges-v3

- Il est conseillé d'utiliser la lib ranges-v3 de Eric Niebler
- Contient encore beaucoup d'utilitaires qui n'ont pas été porté dans la STL

---
layout: intro
class: text-center
---

# Coroutines

---

# Programmation asynchrone

- Les coroutines sont une brique de base de la programmation asynchrone
- Une coroutine est une fonction que l'on peut interrompre
  - Generateur: fonction qui génère des valeurs et rend la main régulièrement
  - I/O: plutot que d'attendre le résultat d'une fonction d'I/O on peut rendre la main au process principal en attendant le résultat
- Concurrence vs parallelisme
  - L'execution dans un thread système se fait en concurrence et en parallèle (multi-taches pré-emptif)
  - L'execution dans une coroutine se fait en concurrence mais pas forcément en parallèle (multi-taches coopératif)
  - Les deux concepts peuvent être combiné

---

# Idée des coroutines en C++

- Un framework bas niveau pour implémenter des cas d'utilisation haut niveau
- Demande plus de détails d'implémentation, mais permet plus de flexibilité
- Le compilateur transforme nos coroutines pour implémenter un certain workflow d'execution
- Objectifs du proposal N4402:
  - Haute performance: permettra à des milliards de coroutines de s'executer en parallèle
  - Permettre aux développeurs de bilbiothèque d'implémenter des lib de coroutine de plus haut niveau (générateurs, goroutines, task executors, etc.)
  - Utilisable dans des environnements sans gestion d'exceptions

---

# Le framework des coroutines en C++

- Le C++ propose un framework permettant d'implémenter des variations de coroutines
- Framework = un cadre d'implémentation a respecter
- Le compilateur transforme notre code pour l'injecter dans le runtime du framework

---

# Elements à implémenter

- La logique d'execution
  - Une fonction utilisant les mots clefs `co_return`, `co_await`, `co_yield`
  - L'appel à cette fonction entraine l'execution du framework et donne accès au client à un handle pour controller les étapes d'execution
- Le coroutine handle
  - Un type encapsulant permettant de manipuler la coroutine depuis l'exterieur
- Le type `promise_type`
  - Un type faisant le pont entre la logique d'execution et le handle
  - Permet de faire passer la data et de spécifier le comportement de mise en pause

---

# Elements géré par le compilateur

- La structure d'execution de la coroutine
- Le stockage des données locales dans un objet lié à la coroutine

---

# Exemple simple: eager future

- Etude du code:
- https://gitlab.com/c2ba-learning/cpp-20/code-template/-/blob/main/src/examples/coroutines/eager_future.cpp

---

# Exemple moins simple: lazy future

- Etude du code:
- https://gitlab.com/c2ba-learning/cpp-20/code-template/-/blob/main/src/examples/coroutines/lazy_future.cpp
- https://gitlab.com/c2ba-learning/cpp-20/code-template/-/blob/main/src/examples/coroutines/lazy_future_in_another_thread.cpp

---

# Exemple de generateur

- Etude du code:
- https://gitlab.com/c2ba-learning/cpp-20/code-template/-/blob/main/src/examples/coroutines/infinite_data_stream.cpp
- https://gitlab.com/c2ba-learning/cpp-20/code-template/-/blob/main/src/examples/coroutines/infinite_data_stream_recurse_coro.cpp

---

# Exemple de job interruptible

- Etude du code:
- https://gitlab.com/c2ba-learning/cpp-20/code-template/-/blob/main/src/examples/coroutines/start_job.cpp
- https://gitlab.com/c2ba-learning/cpp-20/code-template/-/blob/main/src/examples/coroutines/start_job_with_automatic_resume.cpp
- https://gitlab.com/c2ba-learning/cpp-20/code-template/-/blob/main/src/examples/coroutines/start_job_with_automatic_resume_in_thread.cpp

---

# Exemple de synchronisation de thread sender-receiver

- Etude du code:
- https://gitlab.com/c2ba-learning/cpp-20/code-template/-/blob/main/src/examples/coroutines/sender_receiver.cpp

---

# Interface du Promise Object

- Default constructor
- `initial_suspend()`: indique si la coroutine doit se mettre en pause avant execution, renvoit un `Awaitable`
- `final_suspend noexcept()`: indique si la coroutine doit se mettre en pause après execution, renvoit un `Awaitable`
- `unhandled_exception()`: appelée lorsqu'une exception est lancée
- `get_return_object()`: renvoit le coroutine object
- `return_value(value)`: fonction invoquée lorsque le code de la coroutine arrive sur `co_return val`
- `return_void()`: fonction invoquée lorsque le code de la coroutine arrive sur `co_return`
- `yield_value(val)`: fonction invoquée lorsque le code de la coroutine arrive sur `co_yield val`, renvoit un `Awaitable`

---

# Awaitable

- Un `Awaitable` est un objet sur lequel on peut utiliser l'opérateur `co_await`
- Le compilateur remplace:
  - `yield value` par `co_await prom.yield_value(value)`
  - `prom.initial_suspend()` par `co_await prom.initial_suspend()`
  - `prom.final_suspend()` par `co_await prom.final_suspend()`
  - => Donc ces 3 fonctions doivent renvoyer un `Awaitable`
    - `std::suspend_never` et `std::suspend_always` sont des `Awaitable`
  - `prom` est le Promise Object de la coroutine, géré par la compilateur (non visible du développeur)
- Un `Awaitable` doit implémenter:
  - `bool await_ready()`: Indique si le résultat est prêt. Si `false`, `await_suspend` est appelé.
  - `void await_suspend(std::coroutine_handle)`: Programme la coroutine pour être relancée ou détruite
  - `T await_resume()`: Fourni le résultat de `co_await exp` de type `T` (peut être `void`)

---

# `std::suspend_always`, `std::suspend_never`

- La STL fournie deux awaitables de base:
  - `std::suspend_always`: `await_ready` renvoit `false`
  - `std::suspend_never`: `await_ready` renvoit `true`
- On les utilise pour implementer `initial_suspend` et `final_suspend` dans les cas simples

---

# Récapitulatif

- La coroutine factory renvoit un objet de type `X`, `X` étant choisit par le développeur de la coroutine
- `X::promise_type` doit être définit et implémenter le concept de promesse des coroutines.
- Le compilateur va instancier un objet de type `X::promise_type` au moment de la création de la coroutine
- Cet objet doit permettre la création d'un `X` via `X::promise_type::get_return_object()`.
- C'est cet objet qui est manipulé par le code client de la coroutine
- Cet objet va en général avoir pour membre un objet de type `std::coroutine_handle<promise_type>`
- La classe `coroutine_handle` offre les methodes pour reprendre la coroutine, vérifier si elle est terminée, accéder à la promesse associée
- Le role de la classe `X` est finalement d'offrir une interface plus haut niveau et adapté au besoin, par dessus le `coroutine_handle`.
- Lorsque `X::promise_type::final_suspend` n'interrompt pas la coroutine il faut être très vigilant avec l'ownership et la durée de vie des objets.

---
layout: intro
class: text-center
---

# Modules

---

# Pourquoi les modules ?

- Les modules fournissent une alternative aux headers
- Temps de compilation plus court, moins error prone
- Un système plus moderne et plus proche des autres langages
- Avantages:
  - Un module est importé une seule fois même à travers plusieurs unités de compilation
  - Peut importe l'ordre d'import
  - Difficile d'avoir des duplication de symboles
  - Une architecture permettant le découpage en packages logiques

---

# Support des compilateurs

- Malheureusement encore mal supporté par les compilateurs
  - Et encore moins par CMake
- On survolera donc les notions théorique sans les pratiquer
- Il est conseillé d'attendre un meilleur support pour intégrer les modules dans un code de production multi-plateforme

---

# Exemple simple

```cpp
// math.ixx
export module math;
export int add(int fir, int sec){
  return fir + sec;
}
```
- L'extension standard est `.ixx` sous Visual Studio, `.cppm` sous Clang. GCC ne préconise pas d'extension.
- On utilise le mot clef export pour définir le nom du module et les fonctions publiques du module
- Utilisation:
```cpp
#include <iostream>
import math;
int main() {
  std::cout << "math::add(2000, 20): " << math::add(2000, 20) << '\n';
  return 0;
}
```

---

# Exemple plus avancé

```cpp
export module math;
namespace math {
  export int mult(int fir, int sec); // use with math::mult
  export {
    void doTheMath(); // use with math::doTheMath
  }
  export namespace mathDetails {
    int add(int fir, int sec); // use with math::mathDetails::add
  }
  int div(int fir, int sec); // no use outside the module
}
```

---

# Structure d'un module

```cpp
module; // starts the global module fragment

#include <headers for libraries not modularized so far>

export module math; // exporting module declaration; starts the module preamble

import <importing of other modules>

<non-exported declarations> // names only visibile inside the module

export namespace math {
  <exported declarations> // exported names
}
```

---

# Interface et implémentation

- On peut définir uniquement les prototypes dans le fichier d'interface du module `.ixx`
  - Un module ne peut avoir qu'un seul fichier d'interface
  - Les templates doivent être définis dans le fichier d'interface
- On définira l'implémentation des fonctions dans un fichier source classique `.cpp`
  - Un module peut avoir plusieurs fichiers d'implémentation

---

# Header units

- Il est possible de remplacer les includes par des imports:
```cpp
#include <vector>
#include "myHeader.h"
```
- Peut être remplacé par:
```cpp
import <vector>;
import "myHeader.h";
```

---
layout: intro
class: text-center
---

# Autres ajouts du language

---

# Spaceship operator `<=>`

- Permet de définir les 6 opérateurs de comparaison simplement
- Renvoit -1, 0 ou +1
```cpp
struct MyInt {
  int value;
  explicit MyInt(int val): value{val} {}
  auto operator<=>(const MyInt& rhs) const {
    return value < rhs.value ? -1 : value > rhs.value ? 1: 0;
  }
};
```
- On peut également utiliser le spaceship operator d'un autre type:
```cpp
struct MyInt {
  int value;
  explicit MyInt(int val): value{val} {}
  auto operator<=>(const MyInt& rhs) const {
    return value <=> rhs.value;
  }
};
```

---

# Operateurs de comparaison par défaut

- Le compilateur est capable de générer des opérateurs de comparaison par défaut
- Ordre lexicographique sur les membres

```cpp
struct Arrays {
  int ai[1];
  char ac[2];
  float af[3];
  double ad[2][2];
  auto operator<=>(const Arrays&) const = default;
}
```

---

# Designated initialization

- Permet l'initialisation explicite des champs d'une classe
- Repris du C
```cpp
class Point3D{
public:
  int x;
  int y = 12;
  int z = 42;
};

int main() {
  Point3D point1{.x = 0, .y = 1, .z = 2};
  Point3D point2{.x = 0, .z = 20};
}
```
- Les champs doivent être spécifié dans l'ordre de définition
- Les conversions implicites ne sont pas autorisé

---

# `consteval`

- `consteval` permet d'imposer à une fonction l'évaluation à compile time
```cpp
consteval int sqr(int n) {
  return n * n;
}
int main() {
  std::cout << "sqr(5): " << sqr(5) << '\n';
  const int a = 5;
  std::cout << "sqr(a): " << sqr(a) << '\n';
  int b = 5;
  // std::cout << "sqr(b): " << sqr(b) << '\n'; ERROR
}
```

---

# `constinit`

- `constinit` permet d'imposer à une variable statique / thread_local d'être initialisée à compile time
- Permet d'éviter le problème de l'ordre non determinée d'initialisation des variables statiques
- N'implique pas que la variable soit constante
```cpp
constexpr int sqr(int n) {
  return n * n;
}
constinit auto res2 = sqr(5); // On garantie l'évaluation et l'initialisation de la mémoire à compile time
```

---

# Constructeur explicite conditionnel

- On peut spécifier une condition à compile time sur le mot clef `explicit`
- Permet d'autoriser certaines initialisation implicite
```cpp
struct MyBool {
template <typename T>
  explicit(!std::is_same<T, bool>::value) MyBool(T t) {
    std::cout << typeid(t).name() << '\n';
  }
};

void needBool(MyBool b){ }

int main() {
  MyBool myBool1(true);
  MyBool myBool2 = false;
  needBool(myBool1);
  needBool(true);
  // needBool(5); ERROR
  // needBool("true"); ERROR
}
```

---

# Nouveaux non-type template parameters

- Avant C++20, les seuls types autorisé pour des NTTP était:
  - integers et enums
  - pointeurs vers fonctions et attributs de classe, objets statiques
  - lvalue references
  - `std::nullptr_t`
- C++20 autorise:
  - Les types flottants floats et double
  - Les types "litteraux" (objets non mutables)
  - String litterals
    - Permet les regexp à compile time, proposal pour C++23

---

# `[[likely]]` et `[[unlikely]]`

- Attributs permettant d'indiquer au compilateur les branches à optimiser
```cpp
for(size_t i=0; i < v.size(); ++i){
  if (v[i] < 0) [[likely]] sum -= sqrt(-v[i]);
  else sum += sqrt(v[i]);
}
```

---

# Initialisation dans une range-based for loop

```cpp
for (auto vec = std::vector{1, 2, 3}; auto v : vec) {
  std::cout << v << " ";
}

for (auto initList = {1, 2, 3}; auto e : initList) {
  e *= e;
  std::cout << e << " ";
}

using namespace std::string_literals;
for (auto str = "Hello World"s; auto c: str) {
  std::cout << c << " ";
}
```

---
layout: intro
class: text-center
---

# Autres ajouts de la STL

---

# `std::span`

- Une séquence continue en mémoire
  - Tableau C
  - Pointeur + taille
  - `std::vector`, `std::array`, `std::string`
  - Conversion implicite possible depuis ces types
- Pas d'ownership de la donnée, mais modification possible (par opposition à la `std::string_view`)
  - Attention à la durée de vie du conteneur possédant l'ownership
  - Attention à la réallocation
  - `std::span` sur un temporaire = undefined behavior
- Implémente les concepts de `range` et `view`
- Sous-séquences via `.subspan(first, count)`, `.first(count)`, `.last(count)`

---

# Conteneurs et algorithmes utilisables à compile-time

- Tous les conteneurs et algorithmes ont été adapté pour être `constexpr`
- Il est donc possible d'utiliser et manipuler des conteneurs de données connues à compile-time sans cout à runtime
- Use case: calculs de conteneurs/données statiques à compile-time sans passer par de la generation + bake
- Malheureusement ne semble que fonctionner sous GCC en date du 2022-04-12

```cpp
constexpr int maxElement() {
  std::vector myVec = {1, 2, 4, 3};
  std::sort(myVec.begin(), myVec.end());
  return myVec.back();
}
void const_expr_containers() {
  constexpr int maxValue = maxElement();
  static_assert(maxValue == 4);
  constexpr int maxValue2 = [] {
    std::vector myVec = {1, 2, 4, 3};
    std::sort(myVec.begin(), myVec.end());
    return myVec.back();
  }();
  static_assert(maxValue2 == 4);
}

```

---

# `std::to_array`

- Permet la création d'un `std::array` à partir d'un tableau C ou d'une `std::initializer_list`
- Conversion possible

```cpp
auto arr1 = std::to_array("A simple test");
auto arr2 = std::to_array({1, 2, 3, 4, 5});
auto arr3 = std::to_array<double>({0, 1, 3});
auto arr4 = std::to_array<std::pair<int, double>>({
  {1, 0.0}, {2, 5.1}, {3, 5.1}
});
```

---

# `std::make_shared` pour des tableaux

- Permet la création d'un `std::shared_ptr<T[]>` directement avec `std::make_shared`

```cpp
std::shared_ptr<double[]> shar = std::make_shared<double[]>(1024, 1.0);
// Tableau de 1024 doubles initialisés à 1.0
```

---

# `erase` et `erase_if`

- Permet d'éviter d'avoir à utiliser la combinaison `std::remove_if` puis `.erase()` pour retirer des elements d'un conteneur

```cpp
std::erase(cont, val); // Supprime tous les elements x tel que x == val
std::erase_if(cont, pred); // Supprime tous les elements x tel que pred(x) == true
```

---

# `contains`

- Permet d'éviter d'avoir à utiliser `.find()` qui renvoit un itérateur (ou `.count()` qui compte)
- Définit sur les conteneurs associatifs, le test est fait sur la clef (`std::set`, `std::map`, et les unordered associés)

```cpp
std::set<int> mySet{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
assert(mySet.contains(6));
```

---

# `shift_left` et `shift_right`

- Permet de décaller les elements d'un conteneur de `n` elements

```cpp
std::shift_left(begin, end, n);
std::shift_right(being, end, n);
```

---

# `starts_with` et `ends_with`

- Permet de vérifier la présence d'un prefixe ou suffixe sur une `std::string`

```cpp
#include <iostream>
#include <string>

int main() {
  std::cout << std::boolalpha;
  std::string hello_world{"Hello World !"};
  std::cout << hello_world.starts_with("Hello") << '\n';
  std::cout << hello_world.ends_with("World !") << '\n';
  return 0;
}
```

---

# Fonctions de comparaison type safe

- La comparaison avec les opérateur de comparaison `<`, `>`, ... peut entrainer des conversions de type non voulues
- Entraine des résultats incohérent entre `unsigned int` et `int` (on a `(-3 < 7u) == false`)
- Le header `<utility>` défini les fonctions suivantes:
  - `std::cmp_equal`
  - `std::cmp_not_equal`
  - `std::cmp_less`
  - `std::cmp_less_equal`
  - `std::cmp_greater`
  - `std::cmp_greater_equal`
- Ces fonctions donnent des résultats cohérent (on a bien `std::cmp_less(-3, 7u) == true`)

---

# Constantes mathématiques, `midpoint` et `lerp`

- Le header `<numbers>` défini un grand nombre de constantes mathématiques dans le namespace `std::numbers`
- Exemples: `std::numbers::e`, `std::numbers::pi`, `std::numbers::sqrt2`, ...
- `std::midpoint(a, b)` calcule le milieu arithmétique de `a` et `b`
- `std::lerp(a, b, t):` calcule l'interpolation/extrapolation linéaire entre `a` et `b` selon le paramètre `t`

---

# Manipulation binaire avec `<bit>`

- Le header `<bit>` donne accès a un grand nombre d'utilitaire pour la manipulation binaire
- `std::bit_cast`, `std::has_single_bit`, `std::bit_width`, ...

---

# Formattage avec `<format>`

- Alternative safe à `printf`
- Plus user friendly que les stream
- Utilise la syntaxe de python
- Exemple:
```cpp
const auto str = std::format("Hello, C++{}!", "20");
const auto str2 = std::format("{:*>6}", 'x'); // *****x
```
- Malheureusement, uniquement supporté par MSVC en date du 2022-04-12
  - En attendant, la lib prototype `fmt` peut être utilisée

---

# Calendrier et Time Zones

- Ajout d'API pour gérer les timezones et les jours / mois du calendrier
- Encore mal supporté par les compilateurs
- Formattage encore mal supporté par `fmt`
- En attendant une bonne alternative est https://github.com/HowardHinnant/date

---
layout: intro
class: text-center
---

# Mise en Pratique
